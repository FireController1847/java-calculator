package com.firecontroller1847.testapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class App extends Application {

	public Stage stage;
	public CalcController controller;
	public static App app;

	@Override
	public void start(Stage stageIn) throws Exception {
		app = this;
		Thread.currentThread().setName("JavaFX");
		this.stage = stageIn;
		this.stage.initStyle(StageStyle.TRANSPARENT);
		this.stage.setMinHeight(300);
		this.stage.setMinWidth(204);

		FXMLLoader loader = new FXMLLoader(App.class.getClass().getResource("/scenes/main/Main.fxml"));
		controller = new CalcController(stage);
		loader.setController(controller);

		Parent root = loader.load();
		
		Scene scene = new Scene(root);
		scene.setFill(Color.TRANSPARENT);

		stage.setTitle("Calculator");
		stage.setScene(scene);
		ResizeHelper.addResizeListener(this.stage);
		stage.show();
		
		controller.frost(stage);
	}

	public static void main(String[] args) {
		launch(args);
	}

}
