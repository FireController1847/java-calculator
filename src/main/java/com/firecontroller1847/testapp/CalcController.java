package com.firecontroller1847.testapp;

import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class CalcController {

	@FXML
	private Label output;

	@FXML
	private StackPane background;

	@FXML
	private ImageView frost;

	private Stage stage;
	private BigDecimal number = new BigDecimal(0);
	private BigDecimal places;
	private boolean decimal = false;

	private ArrayList<BigDecimal> numberHistory = new ArrayList<BigDecimal>();
	private ArrayList<BigDecimal> actionHistory = new ArrayList<BigDecimal>();
	private String action = "";

	private double xOffset = 0;
	private double yOffset = 0;

	public CalcController(Stage stageIn) {
		stage = stageIn;
	}

	public void update(Stage stageIn) {
		frost(stage);
	}

	/** FROST EFFECT **/
	public void frost(Stage stageIn) {
		frost.setFitWidth(stageIn.getWidth());
		frost.setFitHeight(stageIn.getHeight());
		Image bg = copyBackground(stageIn);
		if (bg == null)
			return;
		frost.setImage(bg);
		frost.setEffect(new BoxBlur(10, 10, 3));
	}

	private Image copyBackground(Stage stageIn) {
		final int X = (int) stageIn.getX();
		final int Y = (int) stageIn.getY();
		final int W = (int) stageIn.getWidth();
		final int H = (int) stageIn.getHeight();
		if (stageIn == null || (X <= 0 || Y <= 0 || W <= 0 || H <= 0))
			return null;
		try {
			Robot robot = new Robot();
			BufferedImage image = robot.createScreenCapture(new java.awt.Rectangle(X, Y, W, H));
			return SwingFXUtils.toFXImage(image, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/** NUMBERS **/

	private void reset() {
		number = new BigDecimal(0);
		places = new BigDecimal(0);
		decimal = false;
		numberHistory.clear();
		actionHistory.clear();
		this.setOutputText("0");
		System.gc();
	}

	private void function(String id, boolean change) {
		if (action != null && change)
			action = id;

		if (actionHistory.size() == 1) {
//			System.out.println("ACTION TO BE MADE: " + action);
//			System.out.println("NUMBER TO BE MODIFIED: " + actionHistory.get(0));

			if (action.equals("add"))
				number = actionHistory.get(0).add(number);
			else if (action.equals("subtract"))
				number = actionHistory.get(0).subtract(number);
			else if (action.equals("divide"))
				number = actionHistory.get(0).divide(number, 1000, RoundingMode.HALF_UP);
			else if (action.equals("multiply"))
				number = actionHistory.get(0).multiply(number);

//			System.out.println("NUMBER TO MAKE THE CHANGE: " + numberHistory.get(numberHistory.size() - 1));
//			System.out.println("OUTPUT: " + number.toString());
			this.setOutputText(number.toString());
			actionHistory.clear();
			actionHistory.add(number);
			if (!id.equals("equal")) {
				numberHistory.clear();
				number = new BigDecimal(0);
			} else {
				number = numberHistory.get(numberHistory.size() - 1);
			}
			return;
		}
		numberHistory.clear();
		decimal = false;
		actionHistory.add(number);
		number = new BigDecimal(0);
		return;
	}

	private void function(String id) {
		function(id, true);
	}

	private void handle(ActionEvent event) {
		String id = ((Button) event.getSource()).getId();

		// ** ACTIONS ** //
		if (id.equals("decimal")) {
			if (decimal)
				return;
			decimal = true;
			places = BigDecimal.valueOf(10);
			this.setOutputText(number.toString() + ".");
			return;
		} else if (id.equals("backspace")) {
			if (numberHistory.size() > 0) {
				numberHistory.remove(numberHistory.size() - 1);
				if (numberHistory.size() > 0)
					number = numberHistory.get(numberHistory.size() - 1);
				else {
					number = new BigDecimal(0);
					decimal = false;
					this.setOutputText("0");
				}
			}
			this.setOutputText(number.toString());
//			System.out.println("AFTR BACKSPACE: " + numberHistory);
			return;
		} else if (id.equals("add")) {
			function(id);
			return;
		} else if (id.equals("subtract")) {
			function(id);
			return;
		} else if (id.equals("divide")) {
			function(id);
			return;
		} else if (id.equals("multiply")) {
			function(id);
			return;
		} else if (id.equals("equal")) {
			function(id, false);
			return;
		} else if (id.equals("ce")) {
			numberHistory.clear();
			decimal = false;
			number = new BigDecimal(0);
			this.setOutputText("0");
		} else if (id.equals("signchange")) {
			number = number.multiply(BigDecimal.valueOf(-1));
		}

		// ** NUMBERS ** //
		else if (id.equals("one")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(1).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(1));
		} else if (id.equals("two")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(2).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(2));
		} else if (id.equals("three")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(3).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(3));
		} else if (id.equals("four")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(4).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(4));
		} else if (id.equals("five")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(5).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(5));
		} else if (id.equals("six")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(6).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(6));
		} else if (id.equals("seven")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(7).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(7));
		} else if (id.equals("eight")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(8).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(8));
		} else if (id.equals("nine")) {
			if (decimal)
				number = number.add(BigDecimal.valueOf(9).divide(places));
			else
				number = number.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(9));
		} else if (id.equals("zero")) {
			if (decimal) {
				places = places.multiply(BigDecimal.valueOf(10));
				this.setOutputText(this.getOutputText() + "0");
				return;
			} else {
				number = number.multiply(BigDecimal.valueOf(10));
			}
		}

		if (decimal)
			places = places.multiply(BigDecimal.valueOf(10));

		numberHistory.add(number);
//		System.out.println("PREV NUMBERS: " + numberHistory);

		this.setOutputText(number.toString());
	}

	@FXML
	private void one(ActionEvent event) {
		handle(event);
//		String txt = fetchCurrentNumber(event);
//		
//		Button src = (Button) event.getSource();
//		System.out.println("ID: " + src.getId());
//		
//		String text = this.getOutputText();
//		if (text.equals("0")) text = "";
//		this.setOutputText(text += "1");
//		
//		System.out.println("NUMBER: " + this.getOutputText());
	}

	@FXML
	private void two(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void three(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void four(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void five(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void six(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void seven(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void eight(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void nine(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void zero(ActionEvent event) {
		handle(event);
	}

	/** FUNCTIONS **/

	@FXML
	private void backspace(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void ce(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void c(ActionEvent event) {
		reset();
	}

	@FXML
	private void divide(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void multiply(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void subtract(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void add(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void equal(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void decimal(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void signchange(ActionEvent event) {
		handle(event);
	}

	@FXML
	private void topbardrag(MouseEvent event) {
		stage.setX(event.getScreenX() - xOffset);
		stage.setY(event.getScreenY() - yOffset);
		update(stage);
	}

	@FXML
	private void topbarpress(MouseEvent event) {
		xOffset = event.getSceneX();
		yOffset = event.getSceneY();
		update(stage);
	}

	@FXML
	private void closewindow(ActionEvent event) {
		System.out.println("CLOSE");
		stage.close();
	}

	@FXML
	private void minmaxwindow(ActionEvent event) {
		System.out.println("MINMAX");
		if (stage.isMaximized())
			stage.setMaximized(false);
		else
			stage.setMaximized(true);
	}

	@FXML
	private void minimizewindow(ActionEvent event) {
		System.out.println("MINIMIZE");
		stage.setIconified(true);
	}

	/** GETTERS AND SETTERS **/

	public BigDecimal getNumber() {
		return number;
	}

	public void setNumber(BigDecimal numberIn) {
		number = numberIn;
	}

	public String getOutputText() {
		return output.getText();
	}

	public void setOutputText(String value) {
		output.setText(value);
	}

}
